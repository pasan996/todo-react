import { Row, Tag } from "antd";
import React, { FunctionComponent } from "react";
import { Todo } from "../../../types/Todo";

interface ITodoCounter {
  todos: Todo[];
  filterTodos: (filter: string | boolean) => void;
}

const TodoCounter: FunctionComponent<ITodoCounter> = ({
  todos,
  filterTodos,
}) => {

  
  const styles = {
    container: {
      borderBottom: "1px solid grey",
      paddingBottom: "1rem",
      marginBottom: "1rem",
    },
  };

  return (
    <Row style={styles.container}>
      <Tag onClick={() => filterTodos("ALL")} color="blue">
        All Tasks : {todos.length}
      </Tag>
      <Tag onClick={() => filterTodos(false)} color="red">
        Incomplete : {todos.filter((todo) => !todo.completed).length}
      </Tag>
      <Tag onClick={() => filterTodos(true)} color="green">
        Complete : {todos.filter((todo) => todo.completed).length}
      </Tag>
    </Row>
  );
};

export default TodoCounter;
