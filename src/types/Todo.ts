export interface Todo {
  id: string;
  description: string;
  completed: boolean;
}

export interface TodoStore {
  todos: Todo[];
  addTodo: (description: string) => void;
  deleteTodo: (id: string) => void;
  toggleComplete: (id: string) => void;
  updateTodo: (todo: Todo) => void;
  removeAllCompleted: () => void;
}


export const ItemTypes = {
  TODO: 'todo',
}
