import { CheckCircleTwoTone, PlusOutlined } from "@ant-design/icons";
import { useDrag, useDrop, DropTargetMonitor } from "react-dnd";
import { XYCoord } from "dnd-core";
import { Button, Checkbox, Col, Form, Input, Row } from "antd";
import React, { FunctionComponent, useRef, useState } from "react";
import { ItemTypes, Todo } from "../../../types/Todo";
import Text from "antd/lib/typography/Text";

interface ITodoItemProps {
  todo: Todo;
  index: number;
  id: string;
  toggleComplete: (id: string) => void;
  deleteTodo: (id: string) => void;
  updateTodo: (todo: Todo) => void;
  moveCard: (dragIndex: number, hoverIndex: number) => void;
}

interface FormValues {
  description: string;
}

interface DragItem {
  index: number;
  id: string;
  type: string;
}

const TodoItem: FunctionComponent<ITodoItemProps> = ({
  todo,
  toggleComplete,
  deleteTodo,
  updateTodo,
  moveCard,
  index,
  id,
}) => {
  const [isUpdate, setIsUpdate] = useState(false);
  const [form] = Form.useForm();

  const onSubmit = (FormValues: FormValues) => {
    const { description } = FormValues;
    updateTodo({ ...todo, description });
    form.resetFields();
    setIsUpdate(false);
  };

  const ref = useRef<HTMLDivElement>(null);
  const [{ handlerId }, drop] = useDrop({
    accept: ItemTypes.TODO,
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    hover(item: DragItem, monitor: DropTargetMonitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect();

      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      // Determine mouse position
      const clientOffset = monitor.getClientOffset();

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex;
    },
  });

  const [{ isDragging }, drag] = useDrag({
    type: ItemTypes.TODO,
    item: () => {
      return { id, index };
    },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  //   const opacity = isDragging ? 0 : 1;
  drag(drop(ref));

  const styles = {
    container: {
      marginBottom: "1rem",
    },
    fontSize: { fontSize: "1.5rem" },
    input: { border: "0", width: "100%", padding: "0rem" },
  };

  return (
    <div style={styles.container} ref={ref} data-handler-id={handlerId}>
      <Row align="middle" justify="start">
        <Col span={24}>
          {!isUpdate ? (
            <Row align="middle" justify="space-between">
              <Col span={1}>
                {!todo.completed ? (
                  <Checkbox
                    onChange={() => toggleComplete(todo.id)}
                    style={styles.fontSize}
                  ></Checkbox>
                ) : (
                  <CheckCircleTwoTone
                    style={styles.fontSize}
                    twoToneColor="#52c41a"
                  />
                )}
              </Col>
              <Col span={17}>
                <Row justify="start" align="middle">
                  <Text style={styles.fontSize}>{todo.description}</Text>
                </Row>
              </Col>
              <Col span={6}>
                <Row align="middle" justify="end">
                  {!todo.completed ? (
                    <Button onClick={() => setIsUpdate(true)} type="link">
                      Edit
                    </Button>
                  ) : null}
                  <Button
                    type="text"
                    onClick={() => deleteTodo(todo.id)}
                    style={{ color: "red" }}
                  >
                    Delete
                  </Button>
                </Row>
              </Col>
            </Row>
          ) : (
            <Form
              initialValues={{ description: todo.description }}
              form={form}
              onFinish={(values) => onSubmit(values)}
            >
              <Form.Item name="description">
                <Input
                  style={styles.input}
                  autoFocus
                  suffix={
                    <>
                      <Button type="link" onClick={() => form.submit()}>
                        Save
                      </Button>
                      <Button type="text" onClick={() => setIsUpdate(false)}>
                        Cancel
                      </Button>
                    </>
                  }
                ></Input>
              </Form.Item>
            </Form>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default TodoItem;
