import { Button, Col, Row } from "antd";
import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useState,
} from "react";
import update from "immutability-helper";

import { useStore } from "../../Store/Todo.store";
import AddTodo from "./Partials/AddTodo";
import TodoItem from "./Partials/TodoItem";
import TodoCounter from "./Partials/TodoCounter";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

const TodoList: FunctionComponent = () => {
  const {
    todos,
    addTodo,
    deleteTodo,
    toggleComplete,
    updateTodo,
    removeAllCompleted,
  } = useStore();

  const [sortedTodos, setSortedTodos] = useState(todos);

  useEffect(() => {
    //this sort method reorder the array to show incompleted tasks on the top
    const sortedArray = todos.sort((a, b) => {
      return Number(a.completed) - Number(b.completed);
    });
    setSortedTodos(sortedArray);

    //saving changes to localstorage to persist the state on refresh
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  //this function filters the tasks depending on the status
  const filterTodos = (filter: string | boolean) => {
    if (filter === "ALL") setSortedTodos(todos);
    else {
      const filteredArray = todos.filter((todo) => todo.completed === filter);
      setSortedTodos(filteredArray);
    }
  };

  //the actual drag and drop function
  const moveCard = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragCard = sortedTodos[dragIndex];
      setSortedTodos(
        update(sortedTodos, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
    },
    [sortedTodos]
  );

  const styles = {
    title: {
      textAlign: "center" as "center",
    },
    containerBox: {
      height: "auto",
      backgroundColor: "white",
      padding: "1rem",
      borderRadius: "10px",
      boxShadow: "5px 10px 18px #888888",
    },
  };

  return (
    <>
      <h1 style={styles.title}>Todo</h1>
      <Row justify="center">
        <Col span={20} style={styles.containerBox}>
          <Row justify="center">
            <Col span={24}>
              <AddTodo addTodo={addTodo}></AddTodo>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <TodoCounter
                filterTodos={filterTodos}
                todos={todos}
              ></TodoCounter>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <DndProvider backend={HTML5Backend}>
                {sortedTodos.map((todo, i) => (
                  <TodoItem
                    key={todo.id}
                    index={i}
                    id={todo.id}
                    todo={todo}
                    toggleComplete={toggleComplete}
                    deleteTodo={deleteTodo}
                    updateTodo={updateTodo}
                    moveCard={moveCard}
                  ></TodoItem>
                ))}
              </DndProvider>
            </Col>
          </Row>
          {todos.length > 0 &&
          todos.filter((todo) => todo.completed).length > 0 ? (
            <Row justify="center">
              <Col span={24}>
                <Row justify="center">
                  <Button
                    onClick={() => removeAllCompleted()}
                    style={{ fontSize: "1rem", color: "orangered" }}
                    type="link"
                  >
                    Clear Completed Tasks
                  </Button>
                </Row>
              </Col>
            </Row>
          ) : null}
        </Col>
      </Row>
    </>
  );
};

export default TodoList;
