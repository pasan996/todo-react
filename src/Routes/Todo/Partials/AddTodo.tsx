import { Input, Form, Button } from "antd";
import {
  PlusCircleFilled,
  PlusCircleOutlined,
  PlusCircleTwoTone,
  PlusOutlined,
} from "@ant-design/icons";
import React, { FunctionComponent } from "react";

interface AddTodoProps {
  addTodo: (description: string) => void;
}

interface FormValues {
  description: string;
}

const AddTodo: FunctionComponent<AddTodoProps> = ({ addTodo }) => {
  const [form] = Form.useForm();

  const onSubmit = (FormValues: FormValues) => {
    const { description } = FormValues;
    addTodo(description);
    form.resetFields();
  };

  const styles = {
    input: { border: "0", width: "100%", padding: "1rem" },
  };

  return (
    <Form form={form} onFinish={(values) => onSubmit(values)}>
      <Form.Item name="description">
        <Input
          style={styles.input}
          placeholder="Create Some Tasks..."
          suffix={
            <Button
              type="primary"
              shape="circle"
              onClick={() => form.submit()}
              icon={<PlusOutlined />}
            ></Button>
          }
        ></Input>
      </Form.Item>
    </Form>
  );
};

export default AddTodo;
