import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import './App.css';
import TodoList from './Routes/Todo/Index';

function App() {
  return (
   <Router>
     <Routes>
       <Route path="/" element={<TodoList/>}></Route>
     </Routes>
   </Router>
  );
}

export default App;
