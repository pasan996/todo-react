import create from "zustand";
import { v4 as uuidv4 } from "uuid";
import { Todo, TodoStore } from "../types/Todo";

export const useStore = create<TodoStore>((set) => ({
  todos: JSON.parse(localStorage.getItem("todos")!) ? JSON.parse(localStorage.getItem("todos")!) : [],
  addTodo: (description: string) => {
    set((state) => ({
      todos: [
        ...state.todos,
        { id: uuidv4(), description, completed: false } as Todo,
      ],
    }));
  },
  deleteTodo: (id: string) => {
    set((state) => ({
      todos: state.todos.filter((todo) => todo.id !== id),
    }));
  },
  toggleComplete: (id: string) => {
    set((state) => ({
      todos: state.todos.map((todo) =>
        todo.id === id
          ? ({ ...todo, completed: !todo.completed } as Todo)
          : todo
      ),
    }));
  },
  updateTodo: (updatedTodo: Todo) => {
    set((state) => ({
      todos: state.todos.map((todo) =>
        todo.id === updatedTodo.id ? updatedTodo : todo
      ),
    }));
  },
  removeAllCompleted: () => {
    set((state) => ({
      todos: state.todos.filter((todo) => !todo.completed),
    }));
  },
}));
